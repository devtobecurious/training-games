import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTrainingSessionsComponent } from '../../components/list-training-sessions/list-training-sessions.component';

@Component({
  selector: 'tgam-training-sessions-page',
  standalone: true,
  imports: [CommonModule, ListTrainingSessionsComponent],
  templateUrl: './training-sessions-page.component.html',
  styleUrls: ['./training-sessions-page.component.css']
})
export class TrainingSessionsPageComponent {

}
