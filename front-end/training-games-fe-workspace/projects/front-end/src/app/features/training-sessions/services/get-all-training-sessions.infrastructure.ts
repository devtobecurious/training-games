import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TrainingSession } from '../models/training-session';

@Injectable({
  providedIn: 'root'
})
export class GetAllTrainingSessionsInfrastructure {
  private http = inject(HttpClient);

  public getAll(): Observable<TrainingSession[]> {
    return this.http.get<TrainingSession[]>('https://localhost:49153/api/trainingsession');
  }
}
