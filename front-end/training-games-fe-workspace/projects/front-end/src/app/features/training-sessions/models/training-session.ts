export interface TrainingSession {
  id: number;
  name: string;
}

export type TrainingSessions = TrainingSession[];
export type TrainingSessionsOrUndefined = TrainingSession[] | undefined;
