import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingSession } from '../../models/training-session';
import { Observable } from 'rxjs';
import { GetAllTrainingSessionsBusiness } from '../../services/get-all-training-sessions.business';

@Component({
  selector: 'tgam-list-training-sessions',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './list-training-sessions.component.html',
  styleUrls: ['./list-training-sessions.component.css']
})
export class ListTrainingSessionsComponent {
  trainingSessionSignal = inject(GetAllTrainingSessionsBusiness).getAll();
}
