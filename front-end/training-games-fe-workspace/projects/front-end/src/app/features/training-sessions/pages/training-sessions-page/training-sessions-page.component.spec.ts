import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingSessionsPageComponent } from './training-sessions-page.component';

describe('TrainingSessionsPageComponent', () => {
  let component: TrainingSessionsPageComponent;
  let fixture: ComponentFixture<TrainingSessionsPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TrainingSessionsPageComponent]
    });
    fixture = TestBed.createComponent(TrainingSessionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
