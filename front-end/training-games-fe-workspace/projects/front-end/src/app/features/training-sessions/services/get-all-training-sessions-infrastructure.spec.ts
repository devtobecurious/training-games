import { TestBed } from '@angular/core/testing';

import { GetAllTrainingSessionsInfrastructure } from './get-all-training-sessions.infrastructure';

describe('GetAllTrainingSessionsInfrastructureService', () => {
  let service: GetAllTrainingSessionsInfrastructure;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetAllTrainingSessionsInfrastructure);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
