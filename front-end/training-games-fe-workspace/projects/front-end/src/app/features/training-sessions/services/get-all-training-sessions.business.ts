import { Injectable, Signal, inject } from '@angular/core';
import { GetAllTrainingSessionsInfrastructure } from './get-all-training-sessions.infrastructure';
import { toSignal } from '@angular/core/rxjs-interop';
import { GetAll } from '../../../core/services/interfaces';
import { TrainingSession, TrainingSessionsOrUndefined } from '../models/training-session';

@Injectable({
  providedIn: 'root'
})
export class GetAllTrainingSessionsBusiness {
  private infra = inject(GetAllTrainingSessionsInfrastructure);

  public getAll(): Signal<TrainingSessionsOrUndefined> {
    return toSignal(this.infra.getAll());
  }
}
