import { Routes } from "@angular/router";
import { TrainingSessionsPageComponent } from "./pages/training-sessions-page/training-sessions-page.component";

export const trainingSessionsRoutes: Routes = [
  {
    path: '',
    component: TrainingSessionsPageComponent
  }
];
