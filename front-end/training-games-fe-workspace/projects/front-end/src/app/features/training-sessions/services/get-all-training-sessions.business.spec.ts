import { TestBed } from '@angular/core/testing';

import { GetAllTrainingSessionsBusiness } from './get-all-training-sessions.business';

describe('GetAllTrainingSessionsBusinessService', () => {
  let service: GetAllTrainingSessionsBusiness;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetAllTrainingSessionsBusiness);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
