import { Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'training-sessions',
    loadChildren: () => import('./features/training-sessions/training-sessions.routes').then(m => m.trainingSessionsRoutes)
  }
];
