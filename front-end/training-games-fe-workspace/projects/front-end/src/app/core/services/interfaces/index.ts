export interface GetAll<T> {
  getAll(): T[];
}
