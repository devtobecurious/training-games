﻿namespace TrainingGames.Core.TrainingSessions.Models
{
    public class TrainingSession
    {
        #region Properties
        public int Id { get; set; }

        public string? Title { get; set; }
        #endregion
    }
}