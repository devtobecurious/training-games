﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingGames.Core.Framework.Interfaces.Services;

namespace TrainingGames.Core.TrainingSessions.Application
{
    /// <summary>
    /// Contract to get all training sessions
    /// </summary>
    public interface IGetAllTrainingSessionsService : IGetAll<TrainingSessions.Models.TrainingSession>
    {
    }
}
