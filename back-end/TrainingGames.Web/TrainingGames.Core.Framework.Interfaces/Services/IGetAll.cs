﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingGames.Core.Framework.Interfaces.Services
{
    /// <summary>
    /// Contract to get all items from application models
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGetAll<T> where T: class
    {
        /// <summary>
        /// Gets all items
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();
    }
}
