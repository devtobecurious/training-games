﻿using Microsoft.EntityFrameworkCore;
using TrainingGames.Core.TrainingSessions.Infrastructure.Data.EntityTypeConfigurations;

namespace TrainingGames.Core.TrainingSessions.Infrastructure.Data
{
    public class TrainingSessionsDbContext : DbContext
    {
        #region Constructors
        protected TrainingSessionsDbContext() { }

        public TrainingSessionsDbContext(DbContextOptions options) : base(options) { }
        #endregion

        #region Internal methods
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TrainingSessionEntityTypeConfiguration());
        }
        #endregion

        #region Properties
        public DbSet<TrainingSessions.Models.TrainingSession> TrainingSessions { get; set; }
        #endregion
    }
}
