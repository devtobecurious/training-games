﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrainingGames.Core.TrainingSessions.Models;

namespace TrainingGames.Core.TrainingSessions.Infrastructure.Data.EntityTypeConfigurations
{
    internal class TrainingSessionEntityTypeConfiguration : IEntityTypeConfiguration<TrainingSession>
    {
        #region Public methods
        public void Configure(EntityTypeBuilder<TrainingSession> builder)
        {
            builder.HasKey(t => t.Id);
            builder.ToTable(nameof(TrainingSession));
        }
        #endregion
    }
}
