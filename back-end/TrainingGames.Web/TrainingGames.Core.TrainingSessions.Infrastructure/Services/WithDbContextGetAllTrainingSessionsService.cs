﻿using TrainingGames.Core.TrainingSessions.Application;
using TrainingGames.Core.TrainingSessions.Infrastructure.Data;
using TrainingGames.Core.TrainingSessions.Models;

namespace TrainingGames.Core.TrainingSessions.Infrastructure.Services
{
    public class WithDbContextGetAllTrainingSessionsService(TrainingSessionsDbContext context) : IGetAllTrainingSessionsService
    {
        #region Public methods
        public IEnumerable<TrainingSession> GetAll()
        {
            return context.TrainingSessions;
        }
        #endregion
    }
}
