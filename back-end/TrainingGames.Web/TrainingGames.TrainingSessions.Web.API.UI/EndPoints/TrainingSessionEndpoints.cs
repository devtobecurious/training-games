﻿using TrainingGames.Core.TrainingSessions.Application;
using TrainingGames.Core.TrainingSessions.Models;

namespace TrainingGames.Web.API.UI.EndPoints;

public static class TrainingSessionEndpoints
{
    #region Public methods
    public static void MapTrainingSessionEndpoints(this IEndpointRouteBuilder routes)
    {
        var group = routes.MapGroup("/api/TrainingSession").WithTags(nameof(TrainingSession));

        group.MapGet("/", (IGetAllTrainingSessionsService service) =>
        {
            return service.GetAll().ToList();
        })
        .WithName("GetAllTrainingSessions")
        .WithOpenApi();

        group.MapGet("/{id}", (int id) =>
        {
            //return new TrainingSession { ID = id };
        })
        .WithName("GetTrainingSessionById")
        .WithOpenApi();

        group.MapPut("/{id}", (int id, TrainingSession input) =>
        {
            return TypedResults.NoContent();
        })
        .WithName("UpdateTrainingSession")
        .WithOpenApi();

        group.MapPost("/", (TrainingSession model) =>
        {
            //return TypedResults.Created($"/api/TrainingSessions/{model.ID}", model);
        })
        .WithName("CreateTrainingSession")
        .WithOpenApi();

        group.MapDelete("/{id}", (int id) =>
        {
            //return TypedResults.Ok(new TrainingSession { ID = id });
        })
        .WithName("DeleteTrainingSession")
        .WithOpenApi();
    }
    #endregion
}
