﻿using Microsoft.EntityFrameworkCore;
using TrainingGames.Core.TrainingSessions.Application;
using TrainingGames.Core.TrainingSessions.Infrastructure.Data;
using TrainingGames.Core.TrainingSessions.Infrastructure.Services;

namespace TrainingGames.TrainingSessions.Web.API.UI.Shared.Services
{
    public static class ServicesExtensionMethods
    {
        #region Public methods
        /// <summary>
        /// Add injection of db contexts
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomDbContexts(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TrainingSessionsDbContext>(options =>
            {
                var connnectionString = configuration.GetConnectionString("TrainingGames.db");

                if (connnectionString == null)
                {
                    throw new ArgumentNullException(connnectionString);
                }
                options.UseMySQL(connnectionString, b => b.MigrationsAssembly("TrainingGames.TrainingSessions.Web.API.UI"));
            });

            return services;
        }

        /// <summary>
        /// Add injection of all services
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IGetAllTrainingSessionsService, WithDbContextGetAllTrainingSessionsService>();

            return services;
        }
        #endregion
    }
}
